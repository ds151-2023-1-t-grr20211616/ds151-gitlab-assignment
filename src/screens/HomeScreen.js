import React from "react"
import { useAuth } from "../context/AuthContext"
import { Button, StyleSheet, Text, TextInput } from "react-native"
import client from "../api/gitlab"

const HomeScreen = () => {

    const { state: { accessToken }, dispatch } = useAuth()
    const [user, setUser] = React.useState('')
    const [password, setPassword] = React.useState('')

    const tryLogin = async () => {
        await client.post('oauth/token', null, {
            params: {
                grant_type: 'password',
                username: user,
                password: password
            }
        })
            .then(response => {
                dispatch({ type: 'login', accessToken: response.data.access_token })
            })
            .catch(error => {
                console.log(error)
            })

        setUser('')
        setPassword('')
    }

    const tryLogout = async () => {
        dispatch({ type: 'logout' })
    }

    // console.log(accessToken)

    if (accessToken !== null && accessToken !== undefined) {
        return (
            <>
                <Text>Home</Text>
                <Button
                    title="Logout"
                    onPress={tryLogout}
                />
            </>
        )
    } else {
        return (
            <>
                <Text>Login</Text>
                <TextInput
                    onChangeText={setUser}
                    value={user}
                    placeholder="Type your user"
                    style={styles.input}
                />
                <TextInput
                    onChangeText={setPassword}
                    value={password}
                    placeholder="Type your password"
                    secureTextEntry={true}
                    style={styles.input}
                />
                <Button
                    onPress={tryLogin}
                    title="Login"
                />
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {

    },
    input: {
        margin: 5,
        padding: 5
    }
})

export default HomeScreen