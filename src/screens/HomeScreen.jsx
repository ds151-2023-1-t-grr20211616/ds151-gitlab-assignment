import { useAuth } from "../context/Auth"
import React, { useState } from "react"
import { Button, StyleSheet, Text, TextInput, View } from "react-native"
import client from "../services/api"
import { useSocialLogin } from "reactjs-social-login"

const HomeScreen = () => {
  const { state: { accessToken }, dispatch } = useAuth()
  const [user, setUser] = React.useState('')
  const [password, setPassword] = React.useState('')

  const tryLogin = async () => {
    await client.post('oauth/token', null, {
      params: {
        grant_type: 'password',
        username: user,
        password: password
      }
    })
      .then(response => {
        dispatch({ type: 'login', accessToken: response.data.access_token })
      })
      .catch(error => {
        console.log(error)
      })

    setUser('')
    setPassword('')
  }

  const tryLogout = async () => {
    dispatch({ type: 'logout' })
  }

  const onSocialLogin = (user) => {
    dispatch({ type: 'login', accessToken: user.accessToken })
  }

  const { GithubLoginButton, GoogleLoginButton } = useSocialLogin({
    providers: ['github', 'google'],
    onLogin: onSocialLogin,
  })

  if (accessToken !== null && accessToken !== undefined) {
    return (
      <View style={styles.container}>
        <Text>HomeScreen</Text>
        <Button
          title="Logout"
          onPress={tryLogout}
        />
      </View>
    )
  } else {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Login</Text>
        <TextInput
          onChangeText={setUser}
          value={user}
          placeholder="Type your user"
          style={styles.input}
        />
        <TextInput
          onChangeText={setPassword}
          value={password}
          placeholder="Type your password"
          secureTextEntry={true}
          style={styles.input}
        />
        <Button
          onPress={tryLogin}
          title="Login"
        />
        <GithubLoginButton title="Login with GitHub" />
        <GoogleLoginButton title="Login with Google" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  input: {
    marginVertical: 5,
    padding: 5,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    width: '100%',
  },
})

export default HomeScreen
